Introducción
============

![Redux](https://raw.githubusercontent.com/reactjs/redux/master/logo/logo-title-dark.png)
============

**Curso de Redux** -- Redux es una librería de JavaScript utilizada para facilitar el desarrollo de aplicaciones web interactivas, por medio del manejo efectivo y centralizado de "estados." El manejo de estados nos permite saber siempre y en un sólo lugar en qué estado se encuentran actualmente nuestros componentes, por ejemplo saber si una imagen está disponible, si está oculta o si ya se está mostrando. En este curso podrás dominar, a través de un proyecto real las capacidades y utilidades de Redux para lograr crear tus propias aplicaciones.

### Implementar Redux en un proyecto de React
#### Comprender a fondo el concepto de reactividad manejado por React