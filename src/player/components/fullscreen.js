import React from 'react';
import FullScreenIcon from '../../icons/components/fullscreen';
import './fullscreen.css';

const FullScreen = (props) => (
	<button className="FullScreen"
		onClick={props.handleFullScreenClick}
	>
		<FullScreenIcon
			size={25}
			color="white"
		/>
	</button>
)

export default FullScreen;