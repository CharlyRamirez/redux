import React from 'react';
import './volume.css';
import VolumenIcon from '../../icons/components/volume';

const Volume = (props) => (
  <button className="Volume">
    <div onClick={props.handleResetVolume}>
      <VolumenIcon
        size={25}
        color="white"
      />
    </div>
    <div className="Volume-range">
      <input
        className="Volume-input"
        type="range"
        min={0}
        max={1}
        step={.05}
        onChange={props.handleVolumeChange}
      />
    </div>
  </button>
)

export default Volume;