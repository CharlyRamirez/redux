import React from 'react';
import leftPad from './leftPad';
import './formattedtime.css'

function formattedTime(props) {
	const minutes = parseInt(props.secs/60, 10);
	const seconds = parseInt(props.secs%60, 10);
	const durationMin = parseInt(props.duration/60, 10);
	const durationSec = parseInt(props.duration%60, 10);

	//console.log('Min: '+minutes, 'Recibe: '+props.secs, 'Seg: '+seconds, 'Duración '+durationMin+':'+durationSec);
	return(
		<div className="Timer">
			<p>
				<span>{leftPad(minutes.toString())}:{leftPad(seconds.toString())}/{leftPad(durationMin.toString())}:{leftPad(durationSec.toString())}</span>
			</p>
		</div>
	)
}

export default formattedTime;