function data(state, action){
    switch(action.type){
        case 'SEARCH_VIDEO':{
            let results = [];
            if(action.payload.query){
                state.data.categories.forEach( category => {
                    results = results.concat(
                        category.playlist.filter(
                            item => item.author
                                .toLowerCase().includes(action.payload.query.toLowerCase())
                        )
                    )
                })
            }
            //const list = state.data.categories[2].playlist;
            /*const results = list.filter((item) => {
                return item.author.includes(action.payload.query)
            })*/
            return {
                ...state,
                search: results
            }
        }
        default:
            return state
    }
}

export default data;