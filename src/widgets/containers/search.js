import React, { Component } from 'react';
import Search from '../components/search';
import { connect } from 'react-redux';


class SearchConatiner extends Component{
	state = {
		value: ''
	}
	hundleSubmit = event => {
		event.preventDefault();
		//console.log(this.input.value);
		//aqui se puede hacer un post para hacer la consulta al servidor
		this.props.dispatch({
			type: 'SEARCH_VIDEO',
			payload: {
				query: this.input.value
			}
		})
	}
	setInputRef = element => {
		this.input = element;
	}
	hundleInputChange = event => {
		this.setState({
			value: event.target.value.replace(' ', '-')
		})
	}
	render(){
		return(
			<Search
				setRef={this.setInputRef}
				hundleSubmit={this.hundleSubmit}
				hundleChange={this.hundleInputChange}
				value={this.state.value}
			/>
		)
	}
}

export default connect()(SearchConatiner);