import React from 'react';
import './search.css';

const Search = (props) => (
	<form
		className="Search"
		onSubmit={props.hundleSubmit}
	>
		<input
			ref={props.setRef}
			type="text"
			className="Search-input"
			placeholder="Busca tu video favorito"
			name="search"
			autoComplete="off"
			onChange={props.hundleChange}
			value={props.value}
		/>
	</form>
)

export default Search;